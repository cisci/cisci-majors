from majors_resources import util, prereq, learningOutcomes, courseOpenings,textBookRequirements, MajorUnits, minorGradInfo, courseListings, UpperDivClasses
import json
import nltk, random, sys
import re

class Majors:
    moduleContributors = ['Andrew Nelson', 'Jeet Shah', 'Conor McDade', 'Anand Mangal', 'Jonathan Lin', 'Austin Robarts', 'Doğaç Efe']
    moduleName = "Majors and Curricula"
    moduleDescription = "This module provides information about Cal Poly's Majors and Curricula.  The data for this" \
        "module is located in /mnt/cisci/modules/majors_resources/."

    numberOfResponses = 0

    def __init__(self):
        self.source_data()

    def source_data(self):
        """
        Read the json files that were previously scraped
        """
        with open('./majors_resources/json_files/exactAnswers.json') as inFile:
            self.answers = json.load(inFile)
        with open('./majors_resources/json_files/courseAnswers.json') as inFile:
            self.answers = self.answers + json.load(inFile)
        with open('./majors_resources/json_files/outcomeAnswers.json') as inFile:
            self.answers = self.answers + json.load(inFile)
        with open('./majors_resources/json_files/courseOpeningsAnswers.json') as inFile:
            self.answers = self.answers + json.load(inFile)
        with open("./majors_resources/json_files/textbookAnswers.json") as inFile:
            self.answers = self.answers + json.load(inFile)
        with open("./majors_resources/json_files/unitsInMajorAnswers.json") as inFile:
            self.answers = self.answers + json.load(inFile)
        with open("./majors_resources/json_files/upperDivClasses.json") as inFile:
            self.answers = self.answers + json.load(inFile)
        with open('./majors_resources/json_files/question_variables.json') as inFile:
            self.variables = json.load(inFile)
        with open('./majors_resources/json_files/minorGradInfo.json') as inFile:
            self.answers = self.answers + json.load(inFile)
        with open('./majors_resources/json_files/courseListing.json') as inFile:
            self.answers = self.answers + json.load(inFile)

    def id(self):
        return [self.moduleName, self.moduleDescription]

    def credits(self):
        return ', '.join(self.moduleContributors)

    def update(self, scrape=True):
        """
        Call whatever scraping routines everyone has written (scraping scripts
        should write their output to some json files) then source those json
        files.
        """
        if scrape:
            print("Scraping data...(this might take a minute or two)")
            prereq.scrape()
            learningOutcomes.scrape()
            courseOpenings.main()
            textBookRequirements.scrape()
            MajorUnits.scrape()
            courseListings.scapeCourseListings()
            UpperDivClasses.scrape()
            minorGradInfo.scrape()

            self.source_data()
            print("Done scraping data...")
        return True

    def response(self,query,history = []):
        match = self.closest_question_match(query)
        brkt_question = match[1]
        dist = match[2]

        parsed_query = self.substitute_variables(query, brkt_question)
        try:
            answer = self.answer_lookup(parsed_query)
            rating = float(len(query) - dist) / float(len(query))
        except:
            answer = "Could not answer question"
            rating = 0.0


        if rating > 0.2:  #signals can be "Normal", "Error", "Question", "Unknown" or "End"
            signal = "Normal"
        else:
            signal = "Error"

        return ([rating, signal, answer])

    def closest_question_match(self, query):
        """
        Anand 

        Decide which question in questions.json matches most closely in
        query.  Probably use edit distance.

        It's okay that the ones in questions.json have [VARIABLE] in them
        while the query has other string text... the edit distance between
        the query and the right question should still be smallest

        Return a tuple of (query, closest_match, edit_distance)
        """
        closest_match = ""
        min_edit_dist = sys.maxsize

        for questionSet in self.answers:
            for entry in questionSet['questions']:
                filtered = self.generate_regex(entry)
                curr_edit_dist = util.edit_dist(query, filtered)
                if curr_edit_dist < min_edit_dist:
                    min_edit_dist = curr_edit_dist
                    closest_match = entry        

        return (query, closest_match, min_edit_dist)

    def generate_regex(self, query):
        query = re.escape(query)
        filtered = re.sub('\\[[^\\]]*\\]', '.*', query)
        filtered = filtered + '{e}'
        #print(filtered)
        return filtered

    def substitute_variables(self, query, bracketed_question):
        vars = []
        ret_recurse = self.substitute_variables_recurse(query, bracketed_question)
        if len(ret_recurse[1][0]) > 0: 
            vars.append(ret_recurse[1])
        while len(ret_recurse[1][0]) != 0:
            ret_recurse = self.substitute_variables_recurse(query, ret_recurse[0])
            if len(ret_recurse[1][0]) > 0: 
                vars.append(ret_recurse[1])
        return (bracketed_question, vars)

    def substitute_variables_recurse(self, query, bracketed_question):
        """
        Dogac
        
        Given query and the closest matching bracketed question (from
        questions.json), figure out what value the variables take.

        Return a tuple of (bracketed_question,
                [list of tuples of (variable_name, variable_value)],
                edit_distance_of_closest_match)
        ("That classes are in [MAJOR] taught by [PROFESSOR]",
                [("MAJORS", "CSC"), ("PROFESSOR", "Foaad Khosmood")],
                5)
        """
        j = open("majors_resources/json_files/question_variables.json")
        variables = json.load(j)
        j = open("majors_resources/json_files/questions.json")
        questions = json.load(j)

        match = re.search('\[(\w*)\]', bracketed_question)
        variable_type = ""
        if match:
            variable_type = match.group(1)

        min_edit_dist = sys.maxsize

        possible_variables = []

        for entry in variables:
            if entry["variable"] == variable_type:
                possible_variables = entry["values"]

        tmp_sub = ""
        chosen_val = ""

        for values in possible_variables:
            for str in values["strings"]:
                tmp_sub = re.sub('\[(.*)\]', str, bracketed_question)
                if self.edit_distance(tmp_sub, query) < min_edit_dist:
                    min_edit_dist = self.edit_distance(tmp_sub, query)
                    chosen_val = str

        var_list = [variable_type, chosen_val]

        return (re.sub('\[(\w*)\]', chosen_val, bracketed_question, 1), var_list)


    def substitute_variables(self, query, bracketed_question):
        """
        Dogac

        Given query and the closest matching bracketed question (from
        questions.json), figure out what value the variables take.

        Return a tuple of (bracketed_question,
                [list of tuples of (variable_name, variable_value)],
                edit_distance_of_closest_match)
        ("That classes are in [MAJOR] taught by [PROFESSOR]",
                [("MAJORS", "CSC"), ("PROFESSOR", "Foaad Khosmood")],
                5)
        """
        match = re.search('.*\[(.*)\].*', bracketed_question)
        variable_type = ""
        if match:
            variable_type = match.group(1)

        min_edit_dist = sys.maxsize

        possible_variables = []

        for entry in self.variables:
            if entry["variable"] == variable_type:
                possible_variables = entry["values"]

        tmp_sub = ""
        chosen_val = ""

        for values in possible_variables:
            for str in values["strings"]:
                tmp_sub = re.sub('\[(.*)\]', str, bracketed_question)
                if util.edit_dist(tmp_sub, query) < min_edit_dist:
                    min_edit_dist = util.edit_dist(tmp_sub, query)
                    chosen_val = values['value']

        var_list = [(variable_type, chosen_val)]

        return (bracketed_question, var_list)

    def answer_lookup(self, processed_question):
        """
        Andrew

        Given a processed question (output from substitute_variables),
        look up the answer in answers.json and return it.

        By this point, the first element in processed_question should
        be an exact match with one of our known questions.
        """
        question = processed_question[0]
        variables = processed_question[1]

        for node in self.answers:
            if question in node['questions']:
                answer = self.retrieve_answer(node, variables)
                return answer

    def retrieve_answer(self, answers, var_vect):
        """
        Given a list of answers (from answers.json), find the one with
        the matching variables
        """
        var_vect = dict(var_vect)

        #lower case the values so that it can match easier
        for key,val in var_vect.items():
            var_vect[key] = val.lower().replace(" ", '')

        for ans in answers['answers']:
            match = False
            for internal_val_and_var in ans['vars']:
                #if the object that looks like
                # internal_val_and_var = {
                #     "val" : "cpe 101",
                #     "var" : "CLASS"
                # }
                #
                # var_vect = {
                #     'CLASS' : 'cpe 101'
                # }

                if internal_val_and_var['var'] in var_vect.keys() and \
                                internal_val_and_var['val'].replace(" ", "").lower() in var_vect.values():
                    match = True
            # The empty case is because of exact answers. Exact answers should not have a variable
            # so if that matches up, then the answer is just the text
            if not ans['vars']:
                match = True
            if match:
                return ans['text']
        #fall back to the one where val is default
        for ans in answers['answers']:
            for val_and_var in ans['vars']:
                if val_and_var['val'] == "default":
                    return ans['text']
        raise Exception("answer not found")


def test():
    myModule = Majors()
    print("module name: ", myModule.id()[0])
    print("module description: ", myModule.id()[1])
    print("credits: ", myModule.credits())
    #print("update results: ", myModule.update(True))
    #query = "What is the difference between Software Engineering and Computer Engineering?"
    #response = myModule.response(query,[])
    #print("rating :", response[0])
    #print("signal :", response[1])
    #if response[1] is not "Error":
    #    print("response: ",response[2])

    history = []
    query = ""
    while True:
        print("How can I help you? (\"quit\" to exit)",end= " ");
        query = input()
        if query.strip().lower() in ['quit', 'exit']:
            break

        response = myModule.response(query,history)
        history.append([query,response])
        if response[1] is "Error":
            print(response[1])
            continue
        else:
            print(response[2])
            print("Confidence:", response[0])


if __name__ == "__main__":
    test()	
