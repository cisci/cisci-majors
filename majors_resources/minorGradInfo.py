import requests
import json
from majors_resources import parseCSV
from bs4 import BeautifulSoup

def findBlendedReqs():
    myRequest = requests.get('https://csc.calpoly.edu/blended-bsms/admission-requirements/')
    soup = BeautifulSoup(myRequest.text, "html.parser")
    outcomes = soup.find(attrs={'id': 'mainLeftFull'})
    tags = outcomes.find('ul').find_all('li')

    learningOutcomes = "The requirements to apply for the blended master program are: \n"
    for line in tags:
        learningOutcomes += line.string + "\n"

    return {"text" : learningOutcomes, 'vars' : []}

def getCSMinorDescription():
    myRequest = requests.get('http://catalog.calpoly.edu/collegesandprograms/collegeofengineering/computerscience/#Computer_Science_Minor')
    soup = BeautifulSoup(myRequest.text, 'html.parser')
    headers = soup.find_all('h2')
    description = 'No minor description found.'

    for header in headers:
        if header.string == 'Computer Science Minor':
            description = header.find_next_sibling('p').text

    return {"text" : description, 'vars' : []}

def getContactInfo():
    myRequest = requests.get('https://csc.calpoly.edu/contact/')
    soup = BeautifulSoup(myRequest.text, 'html.parser')
    info = soup.find(attrs={'id': 'mainLeftFull'})
    infoAnswer = ''
    for text in info.find_all('p'):
        infoAnswer += str('\n'.join(text.find_all(text=True)) + '\n').replace('\u00a0', " ")
    return {'text' : infoAnswer, 'vars' : []}

def getCSMinors():
    myRequest = requests.get('http://www.catalog.calpoly.edu/collegesandprograms/collegeofengineering/computerscience/')
    soup = BeautifulSoup(myRequest.text, 'html.parser')
    table = soup.find(attrs={'class' : 'sc_sctable tbl_programnames'})
    minors = 'These are the current minors offered:\n'
    for row in table.find('tbody').find_all('tr'):
        if 'Minor' in row.find(attrs={'class' : 'column1'}).text:
            minors += row.find(attrs={'class' : 'column0'}).text + '\n'
    return {'text' : minors, 'vars' : []}

def createJSONList(questionMarker, answer, filename):
    questions = parseCSV.parser('majors_resources/questions.csv',
            filt=(lambda x: x[1] == questionMarker))
    questions[0]['answers'] = []
    questions[0]['answers'] = [answer,]
    return questions
    #with open(filename, 'w') as outFile:
    #    json.dump(questions, outFile)

def scrape():

    questions = createJSONList('DepartmentContact', getContactInfo(), 'majors_resources/json_files/departmentContact.json')
    questions += createJSONList('BMSReqs', findBlendedReqs(), 'majors_resources/json_files/bmsReqs.json')
    questions += createJSONList('CSMinorDescription', getCSMinorDescription(), 'majors_resources/json_files/CSMinorDescription.json')
    questions += createJSONList('CSMinors', getCSMinors(), 'majors_resources/json_files/CSMinors.json')


    with open('majors_resources/json_files/minorGradInfo.json', 'w') as outFile:
        json.dump(questions, outFile)

if __name__ == '__main__':
    scrape()
