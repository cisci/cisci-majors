from majors_resources import parseCSV
import json
import os, sys, random, requests
from bs4 import BeautifulSoup
import re

def scrape():
    url = 'http://catalog.calpoly.edu/coursesaz/csc/'
    myRequest = requests.get(url)
    soup = BeautifulSoup(myRequest.text,"html.parser")

    temp = soup.findAll('div',{"class": "courseblock"})
    prereqlist = dict()

    dictionary = dict()
    majorkey = ""
    for val in temp:
        key = ""
        tempdict = dict()
        for course in val.find_all('p',{"class":"courseblocktitle"}):
            text = str(course.get_text())
            text =  text.splitlines()
            subject = text[0]
            subject = subject.split(". ")
            major = subject[0]
            major = major.split("\xa0")
            key = major[1]
            tempdict['major'] = major[0]
            majorkey = major[0]
            tempdict['units'] = text[1]
            tempdict['courseTitle'] = subject[1]

        for coursedesc in val.find_all('div',{"class":"courseblockdesc"}):
            text = coursedesc.get_text()
            #print(text)
            tempdict['courseDescription'] = str(text)

        for prereq in val.find_all('div',{"class":"noindent courseextendedwrap"}):
            text = prereq.get_text()
            text = text.split('Prerequisite:')
            if len(text)>1:
                text = str(text[1])
                #text = text.replace(' '," ")
                #print(text)
                text = text.strip()
                text = text.replace("\xa0"," ")
                tempdict['prerequisite'] = str(text)
                txttemp = re.findall(r'([A-Z]{3,4}\s[0-9]{3})',text)
                for val in txttemp:
                    if val in prereqlist:
                        tempList = list(prereqlist[val])
                        tempStr = str(major[0]) + " " + str(key)
                        tempList.append(str(tempStr))
                        prereqlist[val] = tempList
                    else:
                        tempList = list()
                        tempStr = str(major[0]) + " " + str(key)
                        tempList.append(str(tempStr))
                        prereqlist[val] = tempList
            else:
                tempdict['prerequisite'] ='no prerequisite'

        dictionary[key]= tempdict

    dictionary1 = dict()
    dictionary1[str(majorkey)] =dictionary
    #print(dictionary1)
    # print(prereqlist)
    with open('./majors_resources/json_files/prereqData.json', 'w') as outFile:
        json.dump(dictionary1, outFile)

    prereqs = parseCSV.parser('./majors_resources/questions.csv',
            filt=(lambda x: x[1] == 'Prereqs/PostReqs'))[0]
    descriptions = parseCSV.parser('./majors_resources/questions.csv',
            filt=(lambda x: x[1] == 'Course Goals (objectives)'))[0]

    # print("Now explaining what classes r for")

    prereqs['answers'] = []
    descriptions['answers'] = []

    for course in dictionary1['CSC']:
        info = dictionary1['CSC'][course]
        var = "CSC " + course
        var_vect = [{'var': 'CLASS', 'val': var}]

        prereq_ans = "The prerequisites for %s are %s" % (var, info['prerequisite'])
        prereq_entry = {'vars': var_vect, 'text': prereq_ans}
        prereqs['answers'].append(prereq_entry)

        describe_ans = "%s (%s): \n%s" % (var, info['courseTitle'], info['courseDescription'])
        describe_entry = {'vars': var_vect, 'text': describe_ans}
        descriptions['answers'].append(describe_entry)

    questions = [prereqs, descriptions]
    with open('./majors_resources/json_files/courseAnswers.json', 'w') as outFile:
        json.dump(questions, outFile)

if __name__ == '__main__':
    scrape()
