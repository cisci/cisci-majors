import json
import csv

def add_to_questionSet(question, answer, questionSets):
    spotFound = False
    for node in questionSets:
        if node['answers'][0]['text'] == answer:
            node['questions'].append(question)
            spotFound = True
    if not spotFound:
        newNode = {}
        newNode['questions'] = [question]
        newNode['answers'] = [{'vars': [], 'text': answer}]
        questionSets.append(newNode)
    return questionSets


def parser(filename, filt=None, outFile=None):
    questionSets = []
    if filt is None:
        filt = (lambda x: True)
    dictionary = dict()
    reader = list()
    with open(filename,'r') as csvfile:
        reader = csv.reader(csvfile,delimiter=',')
        for row in reader:
            if filt(row):
                dictionary[str(row[3])]=str(row[6])
                questionSets = add_to_questionSet(row[3], row[6], questionSets)

    #with open('questions.json','w') as outputfile:
    #    json.dump(dictionary,outputfile)
    if outFile:
        with open(outFile, 'w') as outputfile:
            json.dump(questionSets, outputfile)
    return questionSets

def scrape_exact():
    """
    scrape the exact answers (ones where the question and answer are both in
    the questions.csv file)
    """
    parser('majors_resources/questions.csv', filt=(lambda x: x[4] == '0'),
            outFile='majors_resources/exactAnswersNew.json')


if __name__ == '__main__':
    scrape_exact()
