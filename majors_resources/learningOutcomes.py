import requests
import json
from majors_resources import parseCSV
from bs4 import BeautifulSoup

def findOutcomes(url, major):
    myRequest = requests.get(url)
    soup = BeautifulSoup(myRequest.text, "html.parser")
    outcomes = soup.find(attrs={'id': 'textcontainer'})
    tags = outcomes.find('ol').find_all('li')

    learningOutcomes = "The learning outcomes of " + major + " are:\n"
    for line in tags:
        learningOutcomes += line.string + "\n"

    return {"text" : learningOutcomes, 'vars' : [{'var' : 'MAJOR', 'val' : major}]}

def scrape():
    #questions have questions, answers
    questions = parseCSV.parser('majors_resources/questions.csv',
            filt=(lambda x: x[1] == 'Outcomes'))
    questions[0]['answers'] = []
    answers = []

    url = "http://catalog.calpoly.edu/collegesandprograms/collegeofengineering/computerscience/bscomputerscience/"
    answers += [findOutcomes(url, 'CSC'),]
    url = "http://catalog.calpoly.edu/collegesandprograms/collegeofengineering/computerscience/bssoftwareengineering/"
    answers += [findOutcomes(url, 'SE'),]
    url = "http://catalog.calpoly.edu/collegesandprograms/collegeofengineering/computerengineering/bscomputerengineering/"
    answers += [findOutcomes(url, 'CPE'),]

    questions[0]['answers'] = answers
    with open("majors_resources/json_files/outcomeAnswers.json", 'w') as outFile:
        json.dump(questions, outFile)

if __name__ == '__main__':
    scrape()


