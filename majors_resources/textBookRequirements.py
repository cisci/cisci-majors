import json,os
import requests, re
from bs4 import BeautifulSoup
import io
import PyPDF2
try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO



def download_file(download_url):
    """
    This function will download the pdf from the internet and into a local copy
    :param download_url:
    :return:
    """
    response = requests.get(download_url)
    file = open("class_info.pdf", 'wb+')
    file.write(response.content)
    file.close()

def getPDFContent(path):
    """
    http://stackoverflow.com/questions/2481945/how-to-read-line-by-line-in-pdf-file-using-pypdf.
    :param path: The local path to the pdf
    :return: Returns the pdf text in byte string format.
    """
    content = ""
    num_pages = 1
    p = open(path, "rb")
    pdf = PyPDF2.PdfFileReader(p)
    for i in range(0, num_pages):
        content += pdf.getPage(i).extractText() + "\n"
    content = " ".join(content.replace(u"\xa0", " ").strip().split())
    return content.encode("utf-8", "ignore")


def get_textbook(url_to_pdf):
    """
    Given a url (website) to a pdf, this function will go to that website and grab the textbook out of the pdf.
    :param url_to_pdf: Example url: https://csc.calpoly.edu/static/media/uploads/course_outlines/cpe_464.pdf
    :return: Returns the textbook information paragraph.
    """
    download_file(url_to_pdf)
    memory_pdf = io.BytesIO(getPDFContent("class_info.pdf"))
    pdf_text_array = []
    for line in memory_pdf:
        pdf_text_array.extend(re.split("\d[.]{1}", str(line,'utf-8')))
    if len(pdf_text_array) > 4:
        textbook = pdf_text_array[4].replace("Textbook:(and/or oth er requir ed mat erial)","")
    return textbook

def get_json_object(textbook_title, class_title):
    """
    This returns the textbook answer format for a class and textbook.
    :param textbook_title:The textbook title paragraph (not fully formmatted)
    :param class_title: Class title in "csc XXX" format.
    :return: The textbook answer format.
    """
    parsed_title_response = re.sub(' +',' ',textbook_title.replace("Textbook", "")
                                     .replace("(and/or other required material)", ''))

    text_response = "You need " + parsed_title_response + " for " + class_title + "."
    textbook_title_parse = textbook_title.replace("\xa0", "").replace(' ', '').replace("Textbook:", '')
    class_title_no_space = class_title.lower().replace(' ', '')
    if "None" in textbook_title_parse  or len(textbook_title_parse ) == 0:
        text_response = "You don't need a textbook for " + class_title + "."

    json_object = {
        "text": text_response,
        "vars": [
            {
                "val": class_title,
                "var": "CLASS"
            },
            {
                "val" : class_title_no_space,
                "var" : "CLASS"
            }
        ]
    }
    return json_object

def create_json_file(name, json_file):
    file = open(name, "w+")
    json.dump(json_file, file)
    file.close()

def scrape():
    """
    This returns the json object that answers textbook questions.
    :return: Returns the json object
            This function returns all the classes that require textbooks in json format.
    {
     "questions": [
                "Should I get a textbook for [CLASS]?",
                "Should I get a textbook for [CLASS]?",
                "What textbook do I need for [CLASS]?",
               "What is the name of textbook for [CLASS]?",
               "What edition of textbook do I need for [CLASS]?"
    ],
    "answers" : [
            {
                "text" : "You need [TEXTBOOK/nothing] for this class.",
                "vars": [
                    {
                        "val": "CPE 466",
                        "var": "CLASS"
                    }
                ]
            },
            ....
        ]
    }
    """
    url = "https://csc.calpoly.edu/current/expanded-course-outlines/"
    course_outline_url = requests.get(url)
    webpage = BeautifulSoup(course_outline_url.text, 'html.parser')

    main_body = webpage.find(attrs={"id": "mainLeftFull"})
    answer_arr = []
    for link in main_body.find_all('a'):
        #if the link is not empty, then I want to download that pdf
        #and put it into a dictionary of class to
        if link != None:
            url_to_pdf = re.search("href=\"(.*?)\"", str(link)).group(1)
            class_title = re.search("((cpe|csc)_\d{3})", url_to_pdf).group(0).replace("_", " ")
            class_title_csc = class_title.replace("cpe", "csc")
            textbook_title = get_textbook(url_to_pdf)
            ## if it is a CPE class, then I want to cross list it has cpe also.
            if "cpe" in re.search(">(.*?)<", str(link)).group(0).lower():
                json_obj_cpe = get_json_object(textbook_title, class_title_csc.replace("csc", "cpe"))
                answer_arr.append(json_obj_cpe)
            json_obj = get_json_object(textbook_title, class_title_csc)
            answer_arr.append(json_obj)

    json_file = [{
        "questions" : [
            "Should I get a textbook for [CLASS]?",
            "Should I get a textbook for [CLASS]?",
            "What textbook do I need for [CLASS]?",
            "What is the name of textbook for [CLASS]?",
            "What edition of textbook do I need for [CLASS]?"
        ],
        "answers" : answer_arr
    }]
    create_json_file("majors_resources/json_files/textbookAnswers.json", json_file)
    try:
        os.remove("majors_resources/class_info.pdf")
    except OSError:
        pass
