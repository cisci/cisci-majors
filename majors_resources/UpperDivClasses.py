import os, re, requests, json
from bs4 import BeautifulSoup
from majors_resources import MajorUnits

csc_url = "http://www.catalog.calpoly.edu/collegesandprograms/collegeofengineering/computerscience/#courseinventory"
cpe_url = "http://www.catalog.calpoly.edu/collegesandprograms/collegeofengineering/computerengineering/#courseinventory"
se_url = "http://www.catalog.calpoly.edu/collegesandprograms/collegeofengineering/computerscience/#courseinventory"
path_to_json = "majors_resources/json_files/upperDivClasses.json"
questions = [
    "What are all the available upper div classes in [MAJOR]?",
    "What are the upper div classes in [MAJOR]?",
    "What are the upper division classes in [MAJOR]?",
    "What are the upper division units in [MAJOR]?",
    "Can you give me the upper division classes in [MAJOR]?"
    "What are the upper div classes for [MAJOR]?",
    "What are the upper division classes for [MAJOR]?"
]

def scrape():
    se_classes = get_class_titles(se_url)
    csc_classes = get_class_titles(csc_url)
    cpe_classes = get_class_titles(cpe_url)
    answers = []
    answers.append(create_json('csc', csc_classes))
    answers.append(create_json('cpe', cpe_classes))
    answers.append(create_json('se', se_classes))
    create_json_file(answers, path_to_json)



def create_json(major, upper_div_classes):
    """
    This will create a json object that looks like
    {
        "text" : "Upper division classes offered under " + major + " are " + ", ".join(upper_div_classes),
        "vars" : [
            {
                "val": "csc",
                "var": "MAJOR"
            }
        ]
    }
    """
    return {
        "text" : "These are the available upper division classes offered under " + major + ": " + ", ".join(upper_div_classes),
        "vars": [
            {
                "val": major,
                "var": "MAJOR"
            }
        ]
    }


def create_json_file(json_answers, name):
    answers = [
        {
            "answers" : json_answers,
            "questions" : questions
        }
    ]
    file = open(name, "w+")
    json.dump(answers, file)
    file.close()
    return answers

def get_class_titles(course_url):
    course_req = requests.get(course_url)
    soup = BeautifulSoup(course_req.text, "html.parser")

    my_course_listing_request = requests.get(course_url)
    soup = BeautifulSoup(my_course_listing_request.text, "html.parser")
    classes_div = soup.find_all(attrs={"class": "courseblock"})
    course_descrip_lising = {}
    class_titles = []
    for p in classes_div:
        if p.find(attrs={"class": "courseblocktitle"}):
            course_block = p.find(attrs={"class": "courseblocktitle"})
        if course_block.find("strong"):
            class_title = course_block.find("strong")
            if p.find(attrs={"class": "noindent courseextendedwrap"}).find(attrs={"class": "noindent"}):
                class_prereq = p.find(attrs={"class": "noindent courseextendedwrap"}).find(
                    attrs={"class": "noindent"})
                if p.find(attrs={"class": "courseblockdesc"}).find("p"):
                    course_descip = p.find(attrs={"class": "courseblockdesc"}).find("p")
                    class_title = re.search(r"(>)(.*?)([.])", str(class_title)).group(2).lower()\
                        .replace('\xa0','').replace(" ", '')
                    if re.search("[a-z][3-4]{1}", class_title):
                        class_titles.append(class_title)
    return class_titles

scrape()