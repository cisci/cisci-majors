import requests
import json
from majors_resources import parseCSV
from bs4 import BeautifulSoup

def getDSMinorCourseListing():
    myRequest = requests.get('http://catalog.calpoly.edu/collegesandprograms/collegeofsciencemathematics/statistics/crossdisciplinarystudiesminordatascience/')
    soup = BeautifulSoup(myRequest.text, 'html.parser')
    classes = soup.find(attrs={'class': 'sc_courselist'})

    courses = 'Here is the course listing for Data Science:\n'
    for course in classes.find_all('tr'):
        courses += str(' '.join(course.find_all(text=True)) + '\n').replace('\u00a0', " ")
    return {'text' : courses, 'vars' : []}

def getCSMinorCourseListing():
    myRequest = requests.get('http://catalog.calpoly.edu/collegesandprograms/collegeofengineering/computerscience/computerscienceminor/')
    soup = BeautifulSoup(myRequest.text, 'html.parser')
    classes = soup.find(attrs={'class': 'sc_courselist'})

    courses = 'Here is the course listing for the Computing Science Minor:\n'
    for course in classes.find_all('tr'):
        if ('Approved Electives' not in course.text ):
            courses += str(' '.join(course.find_all(text=True)) + '\n').replace('\u00a0', " ")

    return {'text' : courses, 'vars' : []}

def getCIAMinorCourseListing():
    myRequest = requests.get('http://www.catalog.calpoly.edu/collegesandprograms/collegeofengineering/computerscience/computingforinteractiveartsminor/')
    soup = BeautifulSoup(myRequest.text, 'html.parser')
    classes = soup.find(attrs={'class': 'sc_courselist'})

    courses = 'Here is the course listing for Computing Interactive Arts:\n'
    for course in classes.find_all('tr'):
        courses += str(' '.join(course.find_all(text=True)) + '\n').replace('\u00a0', " ")

    return {'text' : courses, 'vars' : []}

def getMastersCourseListing():
    myRequest = requests.get('http://catalog.calpoly.edu/collegesandprograms/collegeofengineering/computerscience/mscomputerscience/')
    soup = BeautifulSoup(myRequest.text, 'html.parser')
    classes = soup.find(attrs={'class': 'sc_courselist'})

    courses = 'Here is the course listing for Computer Science Masters:\n'
    for course in classes.find_all('tr'):
        courses += str(' '.join(course.find_all(text=True)) + '\n').replace('\u00a0', " ")
    return {'text' : courses, 'vars' : []}

def createJSONList(questionMarker, answer, filename):
    questions = parseCSV.parser('majors_resources/questions.csv',
            filt=(lambda x: x[1] == questionMarker))
    questions[0]['answers'] = []
    questions[0]['answers'] = [answer,]
    return questions

def scapeCourseListings():
    questions = createJSONList('CIAMinorCourses', getCIAMinorCourseListing(), 'majors_resources/json_files/CIACourses.json')
    questions += createJSONList('CSMinorCourses', getCSMinorCourseListing(), 'majors_resources/json_files/CSMinorCourses.json')
    questions += createJSONList('DSMinorCourses', getDSMinorCourseListing(), 'majors_resources/json_files/DSMinorCourses.json')
    questions += createJSONList('MastersCourses', getMastersCourseListing(),'majors_resources/json_files/MastersCourses.json')
    with open('majors_resources/json_files/courseListing.json', 'w') as outFile:
        json.dump(questions, outFile)


if __name__ == '__main__':
    scapeCourseListings()