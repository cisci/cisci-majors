from bs4 import BeautifulSoup
import requests
import json
import re

'''
Made by Conor McDade

This answers all questions for the Cal Poly Schedules websites.

To use, import as a module and call the scape method.
The returned value will be a list of objects,
where each object has a list of questions, and a list of variables
with associated answers.

This list can be appended to another list and be
dumped as in json.
'''

class Answer:
    def __init__(self, v, a):
        self.variables = v
        self.answer = a

class Question:
    def __init__(self, q, a):
        self.questions = q
        self.answer = a

def checkRepeatedAnswers(q, answer):
    for x in q:
        if x["answers"] == answer:
            return x
    return -1

def checkAnswers(questions):
    q = []
    for x in range(0, len(questions)):
        tmp = checkRepeatedAnswers(q, questions[x][3].rstrip())
        if tmp is not -1:
            tmp["questions"].append(questions[x][0])
        else:
            tmp = {}
            tmp["questions"] = []
            tmp["questions"].append(questions[x][0])
            tmp["answers"] = questions[x][3]
            q.append(tmp)
    return q

def createQuestions():
    with open('majors_resources/questions.csv') as inFile:
        lines = [line.rstrip('\n') for line in inFile]
    questions = []
    for x in lines:
        parts = x.split(',')
        if parts[0] == 'Conor' and int(parts[5]) != 0:
            questions.append(parts[4:])
    items = checkAnswers(questions)
    return items

def openClass(dep):
    count = 0
    openClass = {}
    for x in dep:
        if int(x[-4]) > int(x[-3]):
            count += 1
    return count

def getNumberLevel(dep, level):
    count = 0
    for x in dep:
        if len(x[0].split()) == 2 and level >= int(x[0].split()[-1]):
            count += 1
    return count

def getMostClassFeature(dep, ndx):
    clss = ""
    most = -1

    for x in dep:
        if int(x[ndx]) > most:
            most = int(x[ndx])
            clss = x[0]
    ret = []
    ret.append(clss)
    ret.append(most)

    return ret

def answerQuestion(var, txt):
    tmp = {}
    tmp["vars"] = var
    tmp["text"] = txt
    return tmp

def checkIfOpen(c):
    return int(c[-3]) < int(c[-4])

def compileClasses(csc, cpe):
    classes = {}
    tmpClasses1 = [x for x in csc if len(x) > 6]
    tmpClasses2 = [x for x in cpe if len(x) > 6]

    for x in tmpClasses1:
        if x[0] not in classes:
            classes[x[0]] = {}
            classes[x[0]]['wait'] = int(x[-2])
            classes[x[0]]['drop'] = int(x[-1])
            if checkIfOpen(x):
                classes[x[0]]['open'] = 1
            else:
                classes[x[0]]['open'] = 0
        else:
            classes[x[0]]['wait'] += int(x[-2])
            classes[x[0]]['wait'] += int(x[-1])

            if checkIfOpen(x):
                classes[x[0]]['open'] += 1

    for x in tmpClasses2:
        if x[0] not in classes:
            classes[x[0]] = {}
            classes[x[0]]['wait'] = int(x[-3])
            classes[x[0]]['drop'] = int(x[-2])
            if checkIfOpen(x):
                classes[x[0]]['open'] = 1
            else:
                classes[x[0]]['open'] = 0
        else:
            classes[x[0]]['wait'] += int(x[-3])
            classes[x[0]]['wait'] += int(x[-4])

            if checkIfOpen(x):
                classes[x[0]]['open'] += 1
    return classes

def getOpenClasses(info):
    classes = []

    for x in info:
        tmp = []
        tmp.append({'var': 'CLASS', 'val': str(x).rstrip()})
        classes.append(answerQuestion(tmp, "There are "+str(info[x]['open'])+" sections of "+str(x)+"still open"))

    tmp = []
    tmp.append({'var': 'CLASS', 'val': 'default'})
    classes.append(answerQuestion(tmp, "That class is not offered this quarter."))

    return classes


def getOffered(info):
    classes = []

    for x in info:
        tmp = []
        tmp.append({'var': 'CLASS', 'val': str(x).rstrip()})
        classes.append(answerQuestion(tmp, "There will be "+str(info[x]['open'])+" sections of "+str(x)+" next quarter."))
    tmp = []
    tmp.append({'var': 'CLASS', 'val': 'default'})
    classes.append(answerQuestion(tmp, "That class will not be offered next quarter."))

    return classes

def getWaitClasses(info):
    classes = []

    for x in info:
        tmp = []
        tmp.append({'var': 'CLASS', 'val': str(x).rstrip()})
        classes.append(answerQuestion(tmp, "There are "+str(info[x]['wait'])+" on the waitlist for "+str(x)))
    return classes

def getDropClasses(info):
    classes = []

    for x in info:
        tmp = []
        tmp.append({'var': 'CLASS', 'val': str(x).rstrip()})
        classes.append(answerQuestion(tmp, str(info[x]['drop'])+" people have dropped "+str(x)))
    return classes

def answerQuestions(questions, csc, cpe, cscNxt, cpeNxt):
    classInfo = compileClasses(csc, cpe)
    nxtClassInfo = compileClasses(cscNxt, cpeNxt)

    for x in questions:
        q = x["questions"]
        a = x["answers"]
        tmp = {}
        multipleAnswers = False
        l = []

        if "How many CSC courses are still open?" in q:
            tmp = answerQuestion([], x["answers"].replace('[NUMBER]', str(openClass(csc))))
        elif "How many CPE courses are still open?" in q:
            tmp = answerQuestion([], a.replace('[NUMBER]', str(openClass(cpe))))
        elif "How many 400 level CSC classes are offered this quarter?" in q:
            tmp = answerQuestion([], a.replace('[NUMBER]', str(getNumberLevel(csc, 400))))
        elif "How many 400 level CSC classes are offered next quarter?" in q:
            tmp = answerQuestion([], a.replace('[NUMBER]', str(getNumberLevel(cscNxt, 400))))
        elif "How many 300 level CPE classes are offered next quarter?" in q:
            tmp = answerQuestion([], a.replace('[NUMBER]', str(getNumberLevel(cpeNxt, 300))))
        elif "How many 300 level CSC classes are offered this quarter?" in q:
            tmp = answerQuestion([], a.replace('[NUMBER]', str(getNumberLevel(csc, 300))))
        elif "How many 400 level CPE classes are offered this quarter?" in q:
            tmp = answerQuestion([], a.replace('[NUMBER]', str(getNumberLevel(cpe, 400))))
        elif "How many 300 level CPE classes are offered this quarter?" in q:
            tmp = answerQuestion([], a.replace('[NUMBER]', str(getNumberLevel(cpe, 300))))
        elif "What is the most waitlisted CPE class?" in q:
            t = getMostClassFeature(cpe, -3)
            s = a.replace('[CLASS]', str(t[0]))
            s = s.replace('[NUMBER]', str(t[1]))
            tmp = answerQuestion([], s)
        elif "What is the most waitlisted CSC class?" in q:
            t = getMostClassFeature(csc, -3)
            s = a.replace('[CLASS]', str(t[0]))
            s = s.replace('[NUMBER]', str(t[1]))
            tmp = answerQuestion([], s)
        elif "What CPE class has been dropped the most?" in q:
            t = getMostClassFeature(cpe, -2)
            s = a.replace('[CLASS]', str(t[0]))
            s = s.replace('[NUMBER]', str(t[1]))
            tmp = answerQuestion([], s)
        elif "What CSC class has been dropped the most?" in q:
            t = getMostClassFeature(csc, -2)
            s = a.replace('[CLASS]', str(t[0]))
            s = s.replace('[NUMBER]', str(t[1]))
            tmp = answerQuestion([], s)
        else:
            multipleAnswers = True

        if "How many sections of [CLASS] are still open?" in q:
            l = getOpenClasses(classInfo)
        elif "How many people have dropped [CLASS]?" in q:
            l = getDropClasses(classInfo)
        elif "What is the waitlist for [CLASS]?" in q:
            l = getWaitClasses(classInfo)

        elif "Is there [CLASS] next quarter?" in q:
            l = getOffered(nxtClassInfo)
        x['answers'] = []
        if multipleAnswers is False:
            x['answers'].append(tmp)
        else:
            for y in l:
                x['answers'].append(y)

    return questions

'''
Parses the table of classes for the department and only pulls out the
necessary information for relevant classes.

The list for each class has the following variables:

'''
def parseDepartment(dep):
    classes = []
    for x in dep:
        tmp = [str(l) for l in x.getText().splitlines() if len(l) > 0 and l != '\xa0']
        classes.append(tmp)
    return classes

def scrape():
    questions = createQuestions()
    csc = requests.get('http://schedules.calpoly.edu/subject_CSC_curr.htm')
    csc_nxt = requests.get('http://schedules.calpoly.edu/subject_CSC_next.htm')
    cpe_nxt = requests.get('http://schedules.calpoly.edu/subject_CPE_next.htm')
    cpe = requests.get('http://schedules.calpoly.edu/subject_CPE_curr.htm')

    soup = BeautifulSoup(csc.text, 'html.parser')
    csc_classes = soup.findAll('tr', {'class': re.compile('entry.*')})
    soup = BeautifulSoup(cpe.text, 'html.parser')
    cpe_classes = soup.findAll('tr', {'class': re.compile('entry.*')})
    soup = BeautifulSoup(csc_nxt.text, 'html.parser')
    csc_nxt_classes = soup.findAll('tr', {'class': re.compile('entry.*')})
    soup = BeautifulSoup(cpe_nxt.text, 'html.parser')
    cpe_nxt_classes = soup.findAll('tr', {'class': re.compile('entry.*')})

    csc = parseDepartment(csc_classes)
    cpe = parseDepartment(cpe_classes)
    cpeNxt = parseDepartment(cpe_nxt_classes)
    cscNxt = parseDepartment(csc_nxt_classes)

    return answerQuestions(questions, csc, cpe, cscNxt, cpeNxt)

def main():
    answers = scrape()
    with open('majors_resources/json_files/courseOpeningsAnswers.json', 'w') as outFile:
        json.dump(answers, outFile)
    #print(json.dumps(answers))

if __name__ == '__main__':
    main()
