#Assumes you have module PyPDF2 to install do pip3 install PyPDF2

import PyPDF2


def pdfParse(filename,password=None,pageNumber=None):
	pdfFileObj = open(filename,'rb')
	pdfReader = PyPDF2.PdfFileReader(pdfFileObj)
	print(pdfReader.numPages)
	if pageNumber==None:
		text = list()
		for i in range(int(pdfReader.numPages)):
			pageObj = pdfReader.getPage(i)
			text.append(pageObj.extractText())

		return text
	else:
		pageObj = pdfReader.getPage(pageNumber)
		return str(pageObj.extractText())




print(pdfParse('Project2S16.pdf'))

