"""
This file needs to be run from the parent directory
"""

import unittest
import json

from majors import Majors
from majors_resources.util import *

class TestEverything(unittest.TestCase):
    #tests are run alphabetically so this forces update to happen first
    def test_0_update(self):
        """
        figure out a way to actually test this... right now we just
        see if it dies
        """
        mod = Majors()
        mod.update(True)

    def test_json_correctness(self):
        """
        make sure all the json files are proper/parsable json
        """
        for fname in ["exactAnswers.json", "question_variables.json"]:
            with open("majors_resources/json_files/" + fname) as ifile:
                data = json.load(ifile)
                self.assertFalse(data is None)

    def test_edit_dist(self):
        self.assertEqual(edit_dist("apple", "apple"), 0)
        self.assertEqual(edit_dist("aple", "apple"), 1)
        self.assertEqual(edit_dist("aplle", "apple"), 1)
        self.assertEqual(edit_dist("", "apple"), 5)

    def test_answer_lookup_with_var(self):
        mod = Majors()

        brkt_string = "What courses are in [MAJOR]?"
        var = ("MAJOR", "CSC")
        dst = 5
        res = mod.answer_lookup((brkt_string, [var], dst))

        self.assertEqual(res,
                "For a list of courses (and descriptions) offered in Computer Science, follow this url: http://catalog.calpoly.edu/coursesaz/CSC")

        var = ("MAJOR", "CPE")
        res = mod.answer_lookup((brkt_string, [var], dst))
        self.assertEqual(res,
                "For a list of courses (and descriptions) offered in Computer Engineering, follow this url: http://catalog.calpoly.edu/coursesaz/CPE")

    def test_answer_lookup_sans_var(self):
        mod = Majors()

        brkt_string = "Are there any concentrations in this department?"
        dst = 5
        res = mod.answer_lookup((brkt_string, [], dst))

        self.assertEqual(res,
                "Yes, there is an Interactive Entertainment concentration in the Computer Science major.");

    def test_answer_lookup_extra_vars(self):
        mod = Majors()

        brkt_string = "Are there any concentrations in this department?"
        var_vect = [("MAJOR", "CSC"), ("PROFESSOR", "Foaad")]
        dst = 5
        res = mod.answer_lookup((brkt_string, var_vect, dst))

        self.assertEqual(res,
                "Yes, there is an Interactive Entertainment concentration in the Computer Science major.")

    def test_top_level(self):
        mod = Majors()
        response = mod.response("Are there any concentrations in this department?", [])

        self.assertEqual(response[2], 
                "Yes, there is an Interactive Entertainment concentration in the Computer Science major.")
        self.assertEqual(response[1], "Normal")

    def test_prereq(self):
        mod = Majors()

        query = "What are the prereqs for CSC357?"
        brkt_string = "What are the prereqs for [CLASS]?"
        res = mod.substitute_variables(query, brkt_string)

        self.assertEqual(res[0], brkt_string)
        self.assertEqual(res[1], [("CLASS", "CSC 357")])
        
        res = mod.answer_lookup(res)
        self.assertEqual(res,
                "The prerequisites for CSC 357 are CSC/CPE 103 with a grade of C- or better, or consent of instructor, and CSC 225 or CPE/EE 229 or CPE/EE 233.")

    def test_substitute_variables(self):
        mod = Majors()

        query = "What courses are in csc?"
        brkt_string = "What courses are in [MAJOR]?"
        res = mod.substitute_variables(query, brkt_string)

        self.assertEqual(res[0], brkt_string)
        self.assertEqual(res[1], [("MAJOR", "csc")])

    def test_substitute_variables_other(self):
        mod = Majors()

        query = "What courses are in Computer Science?"
        brkt_string = "What courses are in [MAJOR]?"
        res = mod.substitute_variables(query, brkt_string)

        self.assertEqual(res[0], brkt_string)
        self.assertEqual(res[1], [("MAJOR", "csc")])
    
    def test_substitute_variables_nearmatch(self):
        mod = Majors()

        query = "What courses are in Computer Sceince?"
        brkt_string = "What courses are in [MAJOR]?"
        res = mod.substitute_variables(query, brkt_string)

        self.assertEqual(res[0], brkt_string)
        self.assertEqual(res[1], [("MAJOR", "csc")])

    def test_substitute_variables_nearmatch_2(self):
        mod = Majors()

        query = "What courses are in Coputer Sceince?"
        brkt_string = "What courses are in [MAJOR]?"
        res = mod.substitute_variables(query, brkt_string)

        self.assertEqual(res[0], brkt_string)
        self.assertEqual(res[1], [("MAJOR", "csc")])

    def test_question_match(self):
        mod = Majors()
        query = "What courses are in Computer Science?"
        res = mod.closest_question_match(query)
        self.assertEqual(res[0], query)
        self.assertEqual(res[1],
                "What courses are in [MAJOR]?")

    def test_edit_dist(self):
        mod = Majors()
        query = "Does CPE357 have pre requisites?"
        answer = "The prerequisites for CSC 357 are CSC/CPE 103 with a grade of C- or better, or consent of instructor, and CSC 225 or CPE/EE 229 or CPE/EE 233."

        res = mod.response(query, [])
        self.assertEqual(res[2], answer)

        query = "Does CPE357 have any prereqs?"
        res2 = mod.response(query, [])
        self.assertEqual(res2[2], answer)

        query = "Duz CPE357 have any prereqs?"
        res3 = mod.response(query, [])
        self.assertEqual(res2[2], answer)

        self.assertTrue(res[0] > res2[0])
        self.assertTrue(res2[0] > res3[0])

    def test_openings(self):
        mod = Majors()
        query = "How many CPE courses are still open?"
        answer = "There are 142 Computer Engineering classes still open."
        res = mod.response(query, [])
        self.assertEqual(res[2], answer)

    def test_describe_course(self):
        mod = Majors()
        query = "Describe CPE357"
        answer = "CSC 357 (Systems Programming.): \n\nC programming language from a system programming perspective.  Standard C language including operators, I/O functions, and data types in the context of system functions.  Unix commands, shell scripting, file system, editors.  3 lectures, 1 laboratory.  Crosslisted as CPE/CSC\u00a0357.\n"
        res = mod.response(query, [])
        self.assertEqual(res[2], answer)

    def test_textbook(self):
        mod = Majors()
        query = "Should I get a textbook for cpe 357?"
        query2 = "What textbook do I need for cpe 357?"
        answer = "You need  Kernighan and Ritchi e, The C Programming Language, 2nd ed, Prentice-Hall, 0131103628 or similar mat erial such as what is contain ed in Stev ens a nd Rago, Ad vanced Prog ramming in the Unix Environm ent, 2nd ed, Addison, 0201433079  for csc 357."
        actual_result = mod.response(query, [])
        actual_result2 = mod.response(query2, [])
        self.assertEqual(answer, str(actual_result[2]))
        self.assertEqual(answer, str(actual_result2[2]))

    def test_upper_units(self):
        mod = Majors()
        query = "How many upper div units are in cpe?"
        answer = "There are 60 upper division units required to complete a B.S. in cpe."
        actual = mod.response(query)
        self.assertEqual(answer, str(actual[2]))

    def test_units(self):
        mod = Majors()
        query = "How many units are in csc?"
        answer = "180-181 total units are required to complete a B.S. in csc."
        actual = mod.response(query)
        self.assertEqual(answer, str(actual[2]))


    def test_units(self):
        mod = Majors()
        query = "What are the upper division classes for software eng?"
        answer = "These are the available upper division classes offered under se: csc300, csc301, csc302, csc303, csc305, csc307, csc308, csc309, csc310, csc311, csc315, csc320, csc321, csc323, csc341, csc344, csc348, csc349, csc350, csc357, csc358, csc365, csc366, csc369, csc371, csc378, csc400, csc402, csc405, csc406, csc409, csc410, csc416, csc419, csc424, csc429, csc430, csc431, csc435, csc436, csc437, csc445, csc448, csc450, csc453, csc454, csc458, csc464, csc465, csc466, csc468, csc471, csc473, csc474, csc476, csc477, csc478, csc479, csc480, csc481, csc483, csc484, csc485, csc486, csc489, csc490, csc491, csc492, csc493, csc494, csc495, csc496"
        actual = mod.response(query)
        self.assertEqual(answer, str(actual[2]))


if __name__ == '__main__':
    unittest.main()
