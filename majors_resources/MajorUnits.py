import re, json, requests
from bs4 import BeautifulSoup

csc_url = "http://catalog.calpoly.edu/collegesandprograms/collegeofengineering/computerscience/bscomputerscience/"
cpe_url = "http://catalog.calpoly.edu/collegesandprograms/collegeofengineering/computerengineering/bscomputerengineering/"
se_url = "http://catalog.calpoly.edu/collegesandprograms/collegeofengineering/computerscience/bssoftwareengineering/"

questions_for_units = [
    "How many total units are required to complete a [MAJOR] degree?",
    "How many units are required to complete [MAJOR]?",
    "How many total units are required to complete a [MAJOR]?",
    "How many units are needed to complete a [MAJOR] degree?",
    "How many units are there in [MAJOR]?",
    "How many units are in [MAJOR]?",
    "Units in [MAJOR]?",
    "How many units does [MAJOR] require?",
    "How many units do I need to graduate with a degree in [MAJOR]?",
    "How many units does it take to graduate from [MAJOR]?",
    "How many units is [MAJOR]?",
    "Number of units for [MAJOR]?"

]

question_for_upper_div = [
    "How many upper division units are needed in [MAJOR]?",
    "How many upper division units are required in [MAJOR]?",
    "How many upper division units are there in [MAJOR]?",
    "How many upper division units are in [MAJOR]?",
    "How many upper division units does [MAJOR] have?",
    "How many upper div units are needed in [MAJOR]?",
    "How many upper div units are required in [MAJOR]?",
    "How many upper div units are there in [MAJOR]?",
    "How many upper div units are in [MAJOR]?",
    "How many upper div units does [MAJOR] have?"
]

def get_total_units(url):
    csc_webpage = BeautifulSoup(url.text, "html.parser")
    total_units = re.search(r">(.*?)<", str(csc_webpage.find(attrs={"class": "listsum"})
                          .find(attrs={"class": "hourscol"}))).group(1)
    return total_units

def get_upper_div_units(url):
    upper_div_units = re.search(r"(\d{1,3}) units of upper division courses", url.text).group(1)
    return str(upper_div_units)

def create_json_file(name, json_file):
    file = open(name, "w+")
    json.dump(json_file, file)
    file.close()


def scrape():
    csc_response = requests.get(csc_url)
    se_response = requests.get(se_url)
    cpe_response = requests.get(cpe_url)
    csc_units = get_total_units(csc_response)
    se_units =   get_total_units(se_response)
    cpe_units = get_total_units(cpe_response)
    csc_upperdiv_units = get_upper_div_units(csc_response)
    se_upperdiv_units = get_upper_div_units(se_response)
    cpe_upperdiv_units = get_upper_div_units(cpe_response)
    answer_arr = []
    answer_arr.append(create_json(csc_units, "csc"))
    answer_arr.append(create_json(se_units, "se"))
    answer_arr.append(create_json(cpe_units, "cpe"))

    answer_for_upper_div = []
    answer_for_upper_div.append(create_upper_div_json(csc_upperdiv_units, "csc"))
    answer_for_upper_div.append(create_upper_div_json(se_upperdiv_units, "se"))
    answer_for_upper_div.append(create_upper_div_json(cpe_upperdiv_units, "cpe"))
    output_json = [
        {
            "questions": questions_for_units,
            "answers": answer_arr
         },
        {
            "questions" : question_for_upper_div,
            "answers" : answer_for_upper_div
        }
    ]
    create_json_file("majors_resources/json_files/unitsInMajorAnswers.json", output_json)




def create_upper_div_json(num_units, major_name):
    return {
        "text" : "There are " + str(num_units) + " upper division units required to complete a B.S. in " + major_name + ".",
        "vars" : [
            {
                "val" : major_name,
                "var" : "MAJOR"
            }
         ]
    }


def create_json(num_units, major_name):
    """
    This will create a json object that looks like
    {
        "text" : "180-181 total units are required to complete a CSC degree",
        "vars" : [
            {
                "val": "csc",
                "var": "MAJOR"
            }
        ]
    }
    """
    return {
        "text" : str(num_units) + " total units are required to complete a B.S. in " + major_name + ".",
        "vars" : [
            {
                "val" : major_name,
                "var" : "MAJOR"
            }
         ]
    }