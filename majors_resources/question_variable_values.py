import os, sys, requests, re
from bs4 import BeautifulSoup
import unicodedata
import json


#[PROFESSOR]
professors_url = "https://csc.calpoly.edu/faculty/"
courses_url = "http://goo.gl/ciqeum"

professors = "PROFESSOR"
department = "DEPARTMENT"
courses = "CLASS"
major = "MAJOR"
ge_area_label = "GE_AREA"
day_of_week = "DAY_OF_WEEK"
cs_minor = "CS_MINOR"

def get_department():
    department_value = {
    "variable": department,
    "values": [ {
                    "strings" : ["computer science department",
                                "csc department",
                                "cs department",
                                "comp sci department",
                                "computer sci department",
                                "computer sciences department"
                                 ],
                    "value" : "csc department"
                }
    ]
}
    return department_value

def get_major():
    csc_array = [
                    "computer science",
                    "csc",
                    "comp sci",
                    "computer sci",
                    "computer sciences"
                ]
    se_array = [
                    "software engineering",
                    "se",
                    "software eng",
                    "soft eng",
                    "software eng",
                    "software engineer"
                ]
    cpe_array = [
        "computer engineering",
        "cpe",
        "computer engineer",
        "comp engineering",
        "computer eng",
        "comp eng"
    ]
    se_array.extend([each.upper() for each in se_array])
    csc_array.extend([each.upper() for each in csc_array])
    cpe_array.extend([each.upper() for each in cpe_array])
    major_values = {
        "variable": major,
        "values": [
            {
                "strings": csc_array,
                "value": "csc"
            },
            {
                "strings": se_array,
                "value": "se"
            },
            {
                "strings": cpe_array,
                "value": "cpe"
            }
        ]
    }
    return major_values

def get_cs_minor():
    """
    This returns the ge dictionary in the format neeeded
     {
        "variable" : cs_minor,
        "values" : [
         {  "value" : 'a1',
           "strings" : ['a1', 'A1']
         }
        ]
     }
    :return:
    """
    cs_minor_values = {
        "variable": cs_minor,
        "values": [
            {
                "strings":
                    [
                        "computer science minor",
                        "csc minor",
                        "comp sci minor",
                        "comp science minor",
                        "computer sci minor",
                        "computer scien minor"
                    ],
                "value": cs_minor
            }
        ]
    }
    return cs_minor_values

def get_days_of_week():
    list_values = []

    day_of_week_values = [
        "monday",
        "tuesday",
        "wednesday",
        "thursday",
        "friday",
        "saturday",
        "sunday"
    ]
    for each in day_of_week_values:
        list_values.append({
            "value": each,
            "strings": list(set([each.lower(), each.replace("day", ""),
                        each[0:3]]))
        })

    weekdays = {
        "variable": day_of_week,
        "values": list_values
    }
    return weekdays


def get_ges():
    '''
    This returns the ge dictionary in the format neeeded
     {
        "variable" : ge_area,
        "values" : [
         {  "value" : 'a1',
           "strings" : ['a1', 'A1']
         }
        ]
     }
    :return:
    '''
    ge_area = [
        "a1",
        "a2",
        "a3",
        "b1",
        "b2",
        "b3",
        "b4",
        "b5",
        "b6",
        "c1",
        "c2",
        "c3",
        "c4",
        "c5",
        "d1",
        "d2",
        "d3",
        "d4",
        "d5"
    ]
    list_values = []
    for each in ge_area:
        list_values.append({
            "value" : each,
            "strings" : [each.upper(), each.lower()]
        })

    ge_area_formatted = {
        "variable" : ge_area_label,
        "values" : list_values
    }
    return ge_area_formatted

def get_courses():
    """
    This will scrape the website for all the course names.
    This should return a json object that says:
    :return:  {variable : "COURSES",
                values: [
                            {
                                "strings": [
                                            "CSC 123",
                                            "CSC123"
                                        ],
                                    "value": "CSC123"
                            },
                            .... continue this list...
                        ]
                }

    """
    my_course_listing_request = requests.get(courses_url)
    soup = BeautifulSoup(my_course_listing_request.text, "html.parser")
    classes_div = soup.find(attrs={"class": "courses"})
    list_jsons = []
    for p in classes_div.find_all(attrs={"class": "courseblocktitle"}):
        class_title = p.find("strong")
        if class_title != None:
            class_name = re.search(r"(>)(.*?)([.])", str(class_title)).group(2)
            class_name = class_name.replace("\xa0", " ")
            single_class_json =\
                {"value"    : class_name,
                 "strings"  : [  class_name,
                                 class_name.replace(" ", ''),
                                 class_name.lower(),
                                 class_name.upper(),
                                 class_name.replace("CSC", "CPE"),
                                 class_name.replace("CSC", "CPE").lower()
                              ]
                }
            list_jsons.append(single_class_json)
    class_values =  {
        "values" : list_jsons,
        "variable" : courses
    }
    return class_values

def generate_professor_prefix():
    """
    Returns a list of professor prefixes
    """
    prefixes = ["Professor", "Prof", "Prof.", "Doctor", "Mr", "Mrs",
                "Mr.", "Mrs.", "Misses", "Mister", "Teacher", "teacher"]
    return prefixes



def get_professors():
    """
    This will scrape the website for all the professors names.
    This should return a json object that says:
    :return:  {variable : "PROFESSORS",
                values: [
                            {
                                "strings": [
                                            "ProfessorZoe Wood",
                                            "DoctorZoe Wood", "Zoe Wood"
                                        ],
                                    "value": "Zoe Wood"
                            },
                            .... continue this list...
                        ]
                }

    """
    professor_request = requests.get(professors_url)
    soup = BeautifulSoup(professor_request.text, "html.parser")
    value_to_strings = {} # this should contain for example : ['Zoe Wood']: "ProfessorZoe Wood", "DoctorZoe Wood", "Zoe Wood"
    list_of_teachers = []
    for name in soup.find_all(attrs={"class": "namecell"}):
        teacher_tag = name.find("a")
        if teacher_tag != None:
            teacher_name = re.search(r"(>)(.*?)(<)", str(teacher_tag)).group(2).replace("\xa0", " ")
            teacher_name = unicodedata.normalize('NFKD', teacher_name)\
                .encode('ascii', 'ignore').decode('ascii')
            teacher_last_name = teacher_name.split(" ")[-1]
            #for each prefix, I want to append a way to say the teachers name. Dr. Zoe wood, teacher Zoe Wood, Misses,
            for prefix in generate_professor_prefix():
                if teacher_name not in value_to_strings:
                    value_to_strings[teacher_name] = []
                value_to_strings[teacher_name].append(prefix + " " + teacher_name)
                value_to_strings[teacher_name].append(prefix + teacher_name)
                value_to_strings[teacher_name].append(prefix + " " + teacher_last_name)
            specific_teacher_dict = {
                    "strings": value_to_strings[teacher_name],
                    "value": teacher_name
            }
            list_of_teachers.append(specific_teacher_dict)
    professors_list_of_jsons = {
        "variable" : professors,
        "values" : list_of_teachers

    }
    return professors_list_of_jsons

def create_entire_json():
    json_object_list = []
    json_object_list.append(get_professors())
    json_object_list.append(get_courses())
    json_object_list.append(get_ges())
    json_object_list.append(get_days_of_week())
    json_object_list.append(get_cs_minor())
    json_object_list.append(get_department())
    json_object_list.append(get_major())
    print(json_object_list)
    file = open("majors_resources/json_files/question_variables.json", "w+")
    json.dump(json_object_list, file)
    file.close()
    return json_object_list


create_entire_json()
# This creates the json file in questions_variables.json. However, the pretty version is
# create_entire_json()

